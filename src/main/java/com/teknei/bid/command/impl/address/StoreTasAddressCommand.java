package com.teknei.bid.command.impl.address;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Specialized component. Executes only the flow that carries the document manager (TAS) process
 */
@Component
public class StoreTasAddressCommand implements Command {
	private static final Logger log = LoggerFactory.getLogger(StoreTasAddressCommand.class);
    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasOperationId;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.idType}")
    private String tasTypeId;
    @Value("${tkn.tas.address}")
    private String tasAddressFile;
    @Autowired
    private TasManager tasManager;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidClieRepository clieRepository;

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".execute ");
        CommandResponse response = new CommandResponse();
        response.setDocumentId(request.getDocumentId());
        response.setScanId(request.getScanId());
        response.setId(request.getId());
        try {
            addAddress(request.getFileContent().get(0), request.getScanId(), request.getDocumentId(), request.getId());
            response.setStatus(Status.ADDRESS_TAS_OK);
            response.setDesc(String.valueOf(Status.ADDRESS_TAS_OK));
        } catch (Exception e) {
            response.setStatus(Status.ADDRESS_TAS_ERROR);
            response.setDesc(String.valueOf(Status.ADDRESS_TAS_ERROR));
        }
        return response;
    }

    /**
     * Adds the address to the document manager
     * @param imageData
     * @param scanId
     * @param idDocumentManager
     * @throws Exception
     */
    private void addAddress(byte[] imageData, String scanId, String idDocumentManager, Long operationId) throws Exception {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".addAddress ");
        Map<String, String> docProperties = getMetadataMapAddress(scanId, operationId);
        tasManager.addDocument(tasAddressFile, idDocumentManager, null, docProperties, imageData, "image/jpeg", "Comprobante de domicilio.jpeg");
    }

    /**
     * Formats the data in order to insert the record
     * @param scanId
     * @return
     * @throws Exception
     */
    private Map<String, String> getMetadataMapAddress(String scanId, Long operationId) throws Exception {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".getMetadataMapAddress ");
        PersonData scanInfo = getPersonalDataFromScan(scanId, operationId);
        Map<String, String> docProperties = new HashMap<>();
        docProperties.put(tasName, scanInfo.getName());
        docProperties.put(tasSurname, scanInfo.getSurename());
        docProperties.put(tasLastname, scanInfo.getSurenameLast());
        docProperties.put(tasIdentification, scanInfo.getPersonalNumber());
        String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
        docProperties.put(tasDate, dateISO8601);
        return docProperties;
    }


    /**
     * Obtains the required info correctly parsed
     * @param scanId
     * @return
     * @throws Exception
     */
    public PersonData getPersonalDataFromScan(String scanId, Long operationId) throws Exception {
    	log.info ("lblancas:: [tkn-service-address] :: "+this.getClass().getName()+".getPersonalDataFromScan ");
        PersonData personalData = new PersonData();
        BidClie bidClie = clieRepository.findOne(operationId);
        personalData.setPersonalNumber(String.valueOf(operationId));
        String name = bidClie.getNomClie() == null ? "" : bidClie.getNomClie();
        String surname = bidClie.getApePate() == null ? "" : bidClie.getApePate();
        String surnamelast = bidClie.getApeMate() == null ? "" : bidClie.getApeMate();
        String surnames = surname + " " + surnamelast;
        personalData.setName(name);
        personalData.setSurename(surname);
        personalData.setSurenameLast(surnamelast);
        personalData.setLastNames(surnames);
        return personalData;
    }
}
