package com.teknei.bid.command;

/**
 * Diferent types of the request
 */
public enum RequestType {

    CREDENTIAL_REQUEST, ADDRESS_REQUEST, FACIAL_REQUEST, BIOM_FINGERS_REQUEST, BIOM_SLAPS_REQUEST;

}