package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Personal data format in order to request document manager services (TAS)
 */
@Data
public class PersonData implements Serializable {

    private String name;
    private String surename;
    private String surenameLast;
    private String lastNames;
    private String personalNumber;

}